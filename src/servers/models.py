# -*- coding: utf-8 -*-

"""
models associated with the servers app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from django.db import models

from utils.models import BaseModel


class Server(BaseModel):
    """ table for servers """
    hostname = models.CharField(max_length=70)
    username = models.CharField(max_length=70)
    port = models.IntegerField()
    ssh_key = models.TextField()
