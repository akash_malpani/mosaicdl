# -*- coding: utf-8 -*-

"""
configuration for the servers app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from django.apps import AppConfig


class ServersConfig(AppConfig):
    """ configuration """
    name = 'servers'
