# -*- coding: utf-8 -*-

"""
serializers associated with the servers app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from servers.models import Server
from utils.serializers import BaseSerializer


class ServerSerializer(BaseSerializer):
    """ serializer for the Servers """

    # pylint: disable=too-few-public-methods,
    class Meta:
        """ metadata associated with Servers serializer """
        model = Server
        fields = '__all__'
