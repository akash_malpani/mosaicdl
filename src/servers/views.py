# -*- coding: utf-8 -*-

"""
apis associated with servers app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from servers import models, serializers
from utils import views


class ServerListView(views.ListView):
    """ Server - list operation """
    # pylint: disable=no-member
    queryset = models.Server.objects.all()
    serializer_class = serializers.ServerSerializer


class ServerCreateView(views.CreateView):
    """ server - create operation """
    serializer_class = serializers.ServerSerializer


class ServerReadView(views.ReadView):
    """ server - read operation """
    # pylint: disable=no-member
    queryset = models.Server.objects.all()
    serializer_class = serializers.ServerSerializer


class ServerUpdateView(views.UpdateView):
    """ server - update operation """
    # pylint: disable=no-member
    queryset = models.Server.objects.all()
    serializer_class = serializers.ServerSerializer


class ServerDeleteView(views.DeleteView):
    """ server - delete operation """
    # pylint: disable=no-member
    queryset = models.Server.objects.all()
