# -*- coding: utf-8 -*-

"""
tests associated with the servers app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

import json

from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase

from servers.models import Server
from utils.mixins import TestMixin


@freeze_time('2018-01-01 00:00:00 +0000')
class ServerTestCase(TestMixin, APITestCase):
    """ test cases for Server """

    def get_server(self):
        """ create Server """
        server_obj = Server()
        server_obj.hostname = 'hostname'
        server_obj.username = 'username'
        server_obj.port = 22
        server_obj.ssh_key = 'ssh-key'
        server_obj.created_by = self.user
        server_obj.last_modified_by = self.user
        server_obj.save()
        return server_obj

    def setUp(self):
        """ set up sample data for neural network"""
        super().setUp()
        self.server = self.get_server()

    def test_list_server(self):
        """ test list operation for Server """

        # hit api
        response = self.client.get('/server/list/')

        # expected response
        expected_response = self.expected_response_list()

        # verify response
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def test_create_server(self):
        """ test create operation for Server """

        # prepare data
        data = {'hostname': 'test_name', 'username': 'test_description',
                'port': 22, 'ssh_key': 'ssh_key'}

        # hit api
        response = self.client.post('/server/create/', data, format='json')

        # verify response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_read_server(self):
        """ test read operation for server """

        # hit api
        response = self.client.get('/server/read/{}/'.format(self.server.id))

        # expected response
        expected_response = self.expected_response_read()

        # verify response
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def test_update_server(self):
        """ test update opeartion for Server """

        # prepare data
        data = {
            'hostname': 'hostname',
            'username': 'something',
            'port': 22,
            'ssh_key': 'blah',
        }

        # hit api
        response = self.client.put('/server/update/{}/'.format(self.server.id),
                                   data, format='json')

        # verify response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete(self):
        """ test delete operation """

        # hit api
        response = self.client.delete('/server/delete/{}/'.format(self.server.id))

        # verify response
        self.assertTrue(response.status_code, status.HTTP_204_NO_CONTENT)

    def expected_response_list(self):
        """ expected response of list operation for Server """
        return json.dumps({
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'created_on': '2018-01-01 00:00:00 +0000',
                'created_by': self.get_user_dict(),
                'hostname': 'hostname',
                'id': str(self.server.id),
                'last_modified_by': self.get_user_dict(),
                'last_modified_on': '2018-01-01 00:00:00 +0000',
                'username': 'username',
                'port': 22,
                'ssh_key': 'ssh-key'
            }]
        })

    def expected_response_read(self):
        """ expected response of read operation for Server """
        return json.dumps({
            'id': str(self.server.id),
            'hostname': 'hostname',
            'username': 'username',
            'port': 22,
            'ssh_key': 'ssh-key',
            'created_on': '2018-01-01 00:00:00 +0000',
            'created_by': self.get_user_dict(),
            'last_modified_on': '2018-01-01 00:00:00 +0000',
            'last_modified_by': self.get_user_dict(),
        })
