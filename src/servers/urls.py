# -*- coding: utf-8 -*-

"""
urlpatterns associated with the servers app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from django.urls import path, re_path

from servers import views


urlpatterns = [
    path('server/list/', views.ServerListView.as_view()),
    path('server/create/', views.ServerCreateView.as_view()),
    re_path('server/read/(?P<pk>[a-z\-0-9]+)/$', views.ServerReadView.as_view()),
    re_path('server/update/(?P<pk>[a-z\-0-9]+)/$', views.ServerUpdateView.as_view()),
    re_path('server/delete/(?P<pk>[a-z\-0-9]+)/$', views.ServerDeleteView.as_view()),
]
