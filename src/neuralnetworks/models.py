# -*- coding: utf-8 -*-

"""
models associated with the neuralnetworks app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from django.db import models

from utils.fields import JSONField
from utils.models import BaseModel


class NeuralNetwork(BaseModel):
    """ table for neuralnetwork """
    name = models.CharField(max_length=70)
    description = models.CharField(max_length=300)
    structure = JSONField()
