# -*- coding: utf-8 -*-

"""
tests associated with the neuralnetworks app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

import json

from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase

from neuralnetworks.models import NeuralNetwork
from utils.mixins import TestMixin


@freeze_time('2018-01-01 00:00:00 +0000')
class NeuralNetworkTestCase(TestMixin, APITestCase):
    """ test cases for neuralnetwork """

    def get_neural_network(self):
        """ create neuralnetwork """
        neural_network_obj = NeuralNetwork()
        neural_network_obj.name = 'Pradnya'
        neural_network_obj.description = 'Software Engineer'
        neural_network_obj.structure = json.dumps({})
        neural_network_obj.created_by = self.user
        neural_network_obj.last_modified_by = self.user
        neural_network_obj.save()
        return neural_network_obj

    def setUp(self):
        """ set up sample data for neural network"""
        super().setUp()
        self.neural_network = self.get_neural_network()

    def test_list_neuralnetwork(self):
        """ test list operation for neuralnetwork """

        # hit api
        response = self.client.get('/neuralnetwork/list/')

        # expected response
        expected_response = self.expected_response_list()

        # verify response
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def test_create_neuralnetwork(self):
        """ test create operation for neuralnetwork """

        # prepare data
        data = {'name': 'test_name', 'description': 'test_description', 'structure': {}}

        # hit api
        response = self.client.post('/neuralnetwork/create/', data, format='json')

        # verify response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_read_neuralnetwork(self):
        """ test read operation for neuralnetwork """

        # hit api
        response = self.client.get('/neuralnetwork/read/{}/'.format(self.neural_network.id))

        # expected response
        expected_response = self.expected_response_read()

        # verify response
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def test_update_neuralnetwork(self):
        """ test update opeartion for neuralnetwork """

        # prepare data
        data = {
            'name': 'pradnya k',
            'description': 'senior Software Engineer',
            'structure': '{}',
            'created_by': self.get_user_dict(),
            'modified_by': self.get_user_dict(),
        }

        # hit api
        response = self.client.put('/neuralnetwork/update/{}/'.format(self.neural_network.id),
                                   data, format='json')

        # verify response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete(self):
        """ test delete operation """

        # hit api
        response = self.client.delete('/neuralnetwork/delete/{}/'.format(self.neural_network.id))

        # verify response
        self.assertTrue(response.status_code, status.HTTP_204_NO_CONTENT)

    def expected_response_list(self):
        """ expected response of list operation for neuralnetwork """
        return json.dumps({
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'created_on': '2018-01-01 00:00:00 +0000',
                'created_by': self.get_user_dict(),
                'description': 'Software Engineer',
                'id': str(self.neural_network.id),
                'last_modified_by': self.get_user_dict(),
                'last_modified_on': '2018-01-01 00:00:00 +0000',
                'name': 'Pradnya',
                'structure': {}
            }]
        })

    def expected_response_read(self):
        """ expected response of read operation for neuralnetwork """
        return json.dumps({
            'id': str(self.neural_network.id),
            'name': 'Pradnya',
            'description': 'Software Engineer',
            'structure': {},
            'created_on': '2018-01-01 00:00:00 +0000',
            'created_by': self.get_user_dict(),
            'last_modified_on': '2018-01-01 00:00:00 +0000',
            'last_modified_by': self.get_user_dict(),
        })
