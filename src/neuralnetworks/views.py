# -*- coding: utf-8 -*-

"""
apis associated with neural networks
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from neuralnetworks import models, serializers
from utils import views


class NeuralNetworkListView(views.ListView):
    """ neuralnetwork - list operation """
    # pylint: disable=no-member
    queryset = models.NeuralNetwork.objects.all()
    serializer_class = serializers.NeuralNetworkSerializer


class NeuralNetworkCreateView(views.CreateView):
    """ neuralnetwork - create operation """
    serializer_class = serializers.NeuralNetworkSerializer


class NeuralNetworkReadView(views.ReadView):
    """ neuralnetwork - read operation """
    # pylint: disable=no-member
    queryset = models.NeuralNetwork.objects.all()
    serializer_class = serializers.NeuralNetworkSerializer


class NeuralNetworkUpdateView(views.UpdateView):
    """ neuralnetwork - update operation """
    # pylint: disable=no-member
    queryset = models.NeuralNetwork.objects.all()
    serializer_class = serializers.NeuralNetworkSerializer


class NeuralNetworkDeleteView(views.DeleteView):
    """ neuralnetwork - delete operation """
    # pylint: disable=no-member
    queryset = models.NeuralNetwork.objects.all()
