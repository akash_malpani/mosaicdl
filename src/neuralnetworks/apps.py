# -*- coding: utf-8 -*-

"""
configuration for the neuralnetworks app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from django.apps import AppConfig


class NeuralNetworkConfig(AppConfig):
    """ configuration """
    name = 'neuralnetworks'
