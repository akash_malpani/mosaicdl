# -*- coding: utf-8 -*-

"""
serializers associated with the neuralnetworks app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from neuralnetworks.models import NeuralNetwork
from utils.serializers import BaseSerializer


class NeuralNetworkSerializer(BaseSerializer):
    """ serializer for neuralnetwork """

    # pylint: disable=too-few-public-methods,
    class Meta:
        """ configuration """
        model = NeuralNetwork
        fields = '__all__'
