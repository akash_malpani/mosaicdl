# -*- coding: utf-8 -*-

"""
urlpatterns associated with the neuralnetworks app
Author: Pradnya Khairnar <pradnya.khairnar@lntinfotech.com>
"""

from django.urls import path, re_path

from neuralnetworks import views


urlpatterns = [
    path('neuralnetwork/list/', views.NeuralNetworkListView.as_view()),
    path('neuralnetwork/create/', views.NeuralNetworkCreateView.as_view()),
    re_path('neuralnetwork/read/(?P<pk>[a-z\-0-9]+)/$', views.NeuralNetworkReadView.as_view()),
    re_path('neuralnetwork/update/(?P<pk>[a-z\-0-9]+)/$', views.NeuralNetworkUpdateView.as_view()),
    re_path('neuralnetwork/delete/(?P<pk>[a-z\-0-9]+)/$', views.NeuralNetworkDeleteView.as_view()),
]
