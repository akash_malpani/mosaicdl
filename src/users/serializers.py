# -*- coding: utf-8 -*-

"""
serializers associated with the users app
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """ serializer for user """

    # pylint: disable=too-few-public-methods,
    class Meta:
        """ configuration """
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name')
