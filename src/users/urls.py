# -*- coding: utf-8 -*-

"""
urlpatterns associated with the users app
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from django.urls import path, re_path

from users import views


urlpatterns = [
    path('user/login/', views.LoginAPIView.as_view()),
    path('user/logout/', views.LogoutAPIView.as_view()),
    path('user/list/', views.UserListView.as_view()),
    path('user/create/', views.UserCreateView.as_view()),
    re_path('user/read/(?P<pk>[0-9]+)/$', views.UserReadView.as_view()),
    re_path('user/update/(?P<pk>[0-9]+)/$', views.UserUpateView.as_view()),
    # re_path('user/delete/(?P<pk>[0-9]+)/$', views.UserDeleteView.as_view()),
]
