# -*- coding: utf-8 -*-

"""
configuration for the users app
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from django.apps import AppConfig


class UsersConfig(AppConfig):
    """ configuration """
    name = 'users'
