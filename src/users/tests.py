# -*- coding: utf-8 -*-

"""
test cases associated with the users app
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from rest_framework.status import HTTP_200_OK
from rest_framework.test import APITestCase

from utils.mixins import TestMixin


class AuthenticationTestCase(TestMixin, APITestCase):
    """ test cases for user authentication """
    perform_login = False

    def expected_response_login(self):
        """ expected response for login """
        user_dict = self.get_user_dict()
        token_key = self.get_token_key()
        return self.to_json({
            'user': user_dict,
            'token': token_key
        })

    def test_user_can_login(self):
        """ test login functionality """

        # prepare data
        data = {'username': 'adam.john', 'password': 'adam.john.007'}

        # hit api
        response = self.client.post('/user/login/', data, format='json')

        # expected output
        expected_response = self.expected_response_login()

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content, expected_response)

    def test_user_can_logout(self):
        """ test logout functionality """

        # prepare data
        auth_header = 'Token {}'.format(self.get_token_key())

        # pylint: disable=no-member
        self.client.credentials(HTTP_AUTHORIZATION=auth_header)

        # hit api
        response = self.client.post('/user/logout/')

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
