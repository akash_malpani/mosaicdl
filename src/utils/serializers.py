# -*- coding: utf-8 -*-

"""
base serializer to be used within the project
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from rest_framework import serializers

from users.serializers import UserSerializer
from utils.fields import JSONField


class BaseSerializer(serializers.ModelSerializer):
    """ base serializer """
    created_by = UserSerializer(read_only=True)
    last_modified_by = UserSerializer(read_only=True)

    def __init__(self, *args, **kwargs):
        """ constructor """
        super().__init__(*args, **kwargs)
        self.serializer_field_mapping.update({
            JSONField: serializers.JSONField
        })
