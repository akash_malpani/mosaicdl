# -*- coding: utf-8 -*-

"""
base model used in mosaicdl
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

import uuid

from django.contrib.auth.models import User
from django.db import models


class BaseModel(models.Model):
    """ base model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    last_modified_on = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE,
                                   related_name='+', editable=False)
    last_modified_by = models.ForeignKey(User, on_delete=models.CASCADE,
                                         related_name='+', editable=False)

    # pylint: disable=too-few-public-methods
    class Meta:
        """ configuration """
        abstract = True
