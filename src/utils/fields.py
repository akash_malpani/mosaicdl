# -*- coding: utf-8 -*-

"""
custom fields used in mosaicdl
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

import json

from django.core.exceptions import ValidationError
from django.db import models


class JSONField(models.TextField):
    """ JSON field implementation on top of django textfield """

    # pylint: disable=unused-argument,no-self-use
    def from_db_value(self, value, expression, connection):
        """ db to python """
        return json.loads(value)

    def get_prep_value(self, value):
        """ python to db """
        try:
            # default value to an empty dictionary
            if not value:
                value = '{}'
            # make sure value is a proper JSON
            value = json.loads(value)
            return json.dumps(value)
        # pylint: disable=unused-variable
        except Exception as ex:
            raise ValidationError(message='invalid JSON string')
