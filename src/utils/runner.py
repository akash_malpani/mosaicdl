# -*- coding: utf-8 -*-

"""
custom test runner for the project
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from django.test.runner import DiscoverRunner
from freezegun import freeze_time


@freeze_time('2018-01-01T00:00:00Z')
class TestRunner(DiscoverRunner):
    """ test runner """
    pass
