# -*- coding: utf-8 -*-

"""
base model used in mosaicdl
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

import json

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from users.serializers import UserSerializer


class TestMixin(object):
    """ mixin provides common functions used in tests """
    maxDiff = None
    perform_login = True

    # pylint: disable=invalid-name
    def setUp(self):
        """ setup sample data """
        self.user = User.objects.filter(username='adam.john').get()
        if self.perform_login is True:
            self.login()

    @staticmethod
    def to_json(data):
        """ convert to json """
        return json.dumps(data, separators=(',', ':'))

    def get_user_dict(self, depth=-1):
        """ convert given user object to dictionary """
        depth += 1
        if depth > 1:
            return self.user.id
        return UserSerializer(self.user).data

    # pylint: disable=no-member
    def get_token_key(self):
        """ retrieve token associated with the given user """
        existing_token = Token.objects.filter(user=self.user).all()
        if not existing_token:
            token = Token(user=self.user)
            token.save()
            return token.key
        return existing_token[0].key

    def login(self):
        """ login to the system """
        auth_header = 'Token {}'.format(self.get_token_key())
        self.client.credentials(HTTP_AUTHORIZATION=auth_header)
