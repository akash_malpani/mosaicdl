# -*- coding: utf-8 -*-

"""
base views to be used within the project
Author: Akhil Lawrence <akhil.lawrence@lntinfotech.com>
"""

from rest_framework import status
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)
from rest_framework.response import Response


class ListView(ListAPIView):
    """ performs list operation on a resource """
    http_method_names = ['get']

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        serializer_class.Meta.depth = 1  # enable nested serialization
        return serializer_class


class CreateView(CreateAPIView):
    """ perform create operation on a resource """
    http_method_names = ['post']

    def perform_create(self, serializer):
        serializer.save(
            created_by=self.request.user,
            last_modified_by=self.request.user
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(status=status.HTTP_201_CREATED)


class ReadView(RetrieveAPIView):
    """ perform read operation on a resource """
    http_method_names = ['get']


class UpdateView(UpdateAPIView):
    """ perform update operation on a resource """
    http_method_names = ['put', 'patch']

    def perform_update(self, serializer):
        """ perform update """
        serializer.save(last_modified_by=self.request.user)

    def update(self, request, *args, **kwargs):
        """ update handler """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, '_prefetched_objects_cache', None):
            # pylint: disable=protected-access
            instance._prefetched_objects_cache = {}
        return Response(status=status.HTTP_200_OK)


class DeleteView(DestroyAPIView):
    """ perform delete operation on a resource """
    http_method_names = ['delete']
